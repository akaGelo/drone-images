#!/bin/bash

mkdir -p $HOME/.m2/

echo 'generate /$HOME/.m2/settings.xml';
cat << EOF > $HOME/.m2/settings.xml

<settings>
  <servers>
     <server>
      <id>$M2_SERVER_ID</id>
      <username>$M2_USERNAME</username>
      <password>$M2_PASSWORD</password>
      <configuration>
        <email>$M2_EMAIL</email>
      </configuration>
   </server>
  </servers>
</settings>


EOF
