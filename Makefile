NAME := akagelo
VERSION := latest
BUILD_ARGS := $(BUILD_ARGS)

all: ansible maven-8 maven-8-node-4

ansible: 
	cd debian-jessie-ansible && docker build $(BUILD_ARGS) -t $(NAME)/debian-jessie-ansible:$(VERSION) .

maven-8: 
	cd debian-jessie-maven3-java-8 && docker build $(BUILD_ARGS) -t $(NAME)/debian-jessie-maven3-java-8:$(VERSION) .
	
maven-8-node-4: 
	cd debian-jessie-maven3-java-8-node-4 && docker build $(BUILD_ARGS) -t $(NAME)/debian-jessie-maven3-java-8-node-4:$(VERSION) .	